from firmata import * 

a = Arduino('/dev/ttyS0', baudrate=57600)
print dir(a)

a.pin_mode(13, firmata.OUTPUT)
a.delay(2)

if __name__ == '__main__':
    while True:
        a.digital_write(13, firmata.HIGH)
        a.delay(.5)
        a.digital_write(13, firmata.LOW)
        a.delay(.5)
        print 'blink'